package com.assignment.parcel.system.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.assignment.parcel.system.dto.ParcelRequestDTO;
import com.assignment.parcel.system.service.ParcelService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {ParcelController.class})
@ExtendWith(SpringExtension.class)
public class ParcelControllerTest {

  @Autowired
  private ParcelController parcelController;

  @MockBean
  private ParcelService parcelService;

  public ParcelRequestDTO parcelRequestDTO = new ParcelRequestDTO(5.0, 10.16, 27.94, 35.56);

  @Test
  void testCalculateDeliveryCost() throws Exception {
    when(parcelService.calculateDeliveryCost(parcelRequestDTO)).thenReturn(0.0);
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
    String requestJson = ow.writeValueAsString(parcelRequestDTO);

    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/parcel/cost")
        .contentType(MediaType.APPLICATION_JSON).content(requestJson);

    MockMvcBuilders.standaloneSetup(parcelController)
        .setControllerAdvice(new ControllerExceptionHandler()).build().perform(requestBuilder)
        .andExpect(status().isOk());
  }

  @Test
  void testCalculateDeliveryCost_WhenDimensionIs0_Then_ReturnBadRequest() throws Exception {
    parcelRequestDTO.setHeight(0.0);
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
    String requestJson = ow.writeValueAsString(parcelRequestDTO);

    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/parcel/cost")
        .contentType(MediaType.APPLICATION_JSON).content(requestJson);

    MockMvcBuilders.standaloneSetup(parcelController)
        .setControllerAdvice(new ControllerExceptionHandler()).build().perform(requestBuilder)
        .andExpect(status().isBadRequest());
  }

  @Test
  void testCalculateDeliveryCost_WhenDimensionIsNull_Then_ReturnBadRequest() throws Exception {
    parcelRequestDTO.setHeight(null);
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
    String requestJson = ow.writeValueAsString(parcelRequestDTO);

    MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/parcel/cost")
        .contentType(MediaType.APPLICATION_JSON).content(requestJson);

    MockMvcBuilders.standaloneSetup(parcelController)
        .setControllerAdvice(new ControllerExceptionHandler()).build().perform(requestBuilder)
        .andExpect(status().isBadRequest());
  }

}
