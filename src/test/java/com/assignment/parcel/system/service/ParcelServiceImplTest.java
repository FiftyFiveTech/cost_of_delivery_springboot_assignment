package com.assignment.parcel.system.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import com.assignment.parcel.system.constants.ParcelConditionStatus;
import com.assignment.parcel.system.dto.ParcelRequestDTO;
import com.assignment.parcel.system.exception.PackageOverweightException;
import com.assignment.parcel.system.model.Parcel;
import com.assignment.parcel.system.model.Parcel.Dimensions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {ParcelServiceImpl.class})
@ExtendWith(SpringExtension.class)
public class ParcelServiceImplTest {

  @Autowired
  private ParcelServiceImpl parcelService;

  @MockBean
  private ParcelConditionCheckerService parcelConditionCheckerService;

  @MockBean
  private ParcelCostCalculatorService parcelCostCalculatorService;

  public ParcelRequestDTO parcelRequestDTO = new ParcelRequestDTO(5.0, 10.16, 27.94, 35.56);
  public Parcel parcel = new Parcel(new Dimensions(5.0, 10.16, 35.56, 27.94));

  @Test
  void testCalculateParcelCost() {
    when(parcelConditionCheckerService.checkParcelCondition(parcel)).thenReturn(
        ParcelConditionStatus.HEAVY_PARCEL);
    when(parcelCostCalculatorService.calculate(parcel,
        ParcelConditionStatus.HEAVY_PARCEL)).thenReturn(100.0);
    assertEquals(100.0, parcelService.calculateDeliveryCost(parcelRequestDTO));
  }

  @Test
  void testCalculateParcelCost_WhenParcelConditionStatusIsReject_Then_ThrowPackageOverweightException() {
    when(parcelConditionCheckerService.checkParcelCondition(parcel)).thenReturn(
        ParcelConditionStatus.REJECT);
    assertThrows(PackageOverweightException.class,
        () -> parcelService.calculateDeliveryCost(parcelRequestDTO));
  }
}
