package com.assignment.parcel.system.controller;

import com.assignment.parcel.system.constants.ExceptionMessage;
import com.assignment.parcel.system.dto.BaseResponseDTO;
import com.assignment.parcel.system.exception.PackageOverweightException;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler {

  @ExceptionHandler(MethodArgumentNotValidException.class)
  protected ResponseEntity<BaseResponseDTO> handleMethodArgumentNotValidException(
      MethodArgumentNotValidException ex) {
    List<String> errors = ex.getBindingResult().getFieldErrors().stream()
        .map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList());

    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
        .body(new BaseResponseDTO(HttpStatus.BAD_REQUEST.value(), errors.get(0)));
  }

  @ExceptionHandler(HttpMessageNotReadableException.class)
  protected ResponseEntity<BaseResponseDTO> handleHttpMessageNotReadableException(
      HttpMessageNotReadableException ex) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
        .body(new BaseResponseDTO(HttpStatus.BAD_REQUEST.value(),
            ExceptionMessage.NUMERIC_DIMENSIONS));
  }

  @ExceptionHandler(PackageOverweightException.class)
  protected ResponseEntity<BaseResponseDTO> handleParcelOverweightException(
      PackageOverweightException ex) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
        .body(new BaseResponseDTO(HttpStatus.BAD_REQUEST.value(), ex.getMessage()));
  }

}
