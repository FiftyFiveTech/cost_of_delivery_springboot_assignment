package com.assignment.parcel.system.controller;


import com.assignment.parcel.system.dto.DeliveryCostResponseDTO;
import com.assignment.parcel.system.dto.ParcelRequestDTO;
import com.assignment.parcel.system.service.ParcelService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/parcel")
public class ParcelController {

  @Autowired
  private ParcelService parcelService;

  @PostMapping("/cost")
  public ResponseEntity<DeliveryCostResponseDTO> calculateDeliveryCost(
      @Valid @RequestBody ParcelRequestDTO parcelRequestDTO) {
    return new ResponseEntity<>(
        new DeliveryCostResponseDTO(parcelService.calculateDeliveryCost(parcelRequestDTO)),
        HttpStatus.OK);
  }
}
