package com.assignment.parcel.system.exception;

public class PackageOverweightException extends RuntimeException {

  public PackageOverweightException(String message) {
    super(message);
  }
}
