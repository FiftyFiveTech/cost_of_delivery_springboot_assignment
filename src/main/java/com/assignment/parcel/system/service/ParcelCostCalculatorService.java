package com.assignment.parcel.system.service;

import com.assignment.parcel.system.constants.ParcelConditionStatus;
import com.assignment.parcel.system.model.Parcel;

public interface ParcelCostCalculatorService {

  Double calculate(Parcel parcel, ParcelConditionStatus parcelConditionStatus);

}
