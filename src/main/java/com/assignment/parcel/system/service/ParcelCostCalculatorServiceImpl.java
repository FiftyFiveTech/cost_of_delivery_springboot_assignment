package com.assignment.parcel.system.service;

import com.assignment.parcel.system.config.ParcelConditionsConfig;
import com.assignment.parcel.system.constants.ParcelConditionStatus;
import com.assignment.parcel.system.model.Parcel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParcelCostCalculatorServiceImpl implements
    ParcelCostCalculatorService {

  @Autowired
  private ParcelConditionsConfig parcelConditionsConfig;

  @Override
  public Double calculate(Parcel parcel, ParcelConditionStatus parcelConditionStatus) {
    switch (parcelConditionStatus) {
      case HEAVY_PARCEL:
        return parcelConditionsConfig.getHeavyParcelUnitPrice() * parcel.getDimensions()
            .getWeight();
      case SMALL_PARCEL:
        return parcelConditionsConfig.getSmallParcelUnitPrice() * parcel.getDimensions()
            .getVolume();
      case MEDIUM_PARCEL:
        return parcelConditionsConfig.getMediumParcelUnitPrice() * parcel.getDimensions()
            .getVolume();
      case LARGE_PARCEL:
        return parcelConditionsConfig.getLargeParcelUnitPrice() * parcel.getDimensions()
            .getVolume();
      default:
        return null;
    }
  }

}
