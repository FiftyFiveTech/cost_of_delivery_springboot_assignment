package com.assignment.parcel.system.service;

import com.assignment.parcel.system.dto.ParcelRequestDTO;
import com.assignment.parcel.system.exception.PackageOverweightException;

public interface ParcelService {

  /**
   * @param parcelRequestDto consists the dimensions of the parcel
   * @return parcel cost in Double
   * @throws PackageOverweightException when parcelConditionStatus is REJECT which means package
   *                                    does not meet the required conditions
   */
  Double calculateDeliveryCost(ParcelRequestDTO parcelRequestDto) throws PackageOverweightException;
}
