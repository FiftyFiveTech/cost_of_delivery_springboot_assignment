package com.assignment.parcel.system.service;

import com.assignment.parcel.system.constants.ExceptionMessage;
import com.assignment.parcel.system.constants.ParcelConditionStatus;
import com.assignment.parcel.system.dto.ParcelRequestDTO;
import com.assignment.parcel.system.exception.PackageOverweightException;
import com.assignment.parcel.system.model.Parcel;
import com.assignment.parcel.system.model.Parcel.Dimensions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParcelServiceImpl implements ParcelService {

  @Autowired
  private ParcelConditionCheckerService parcelConditionCheckerService;

  @Autowired
  private ParcelCostCalculatorService parcelCostCalculatorService;

  @Override
  public Double calculateDeliveryCost(ParcelRequestDTO parcelRequestDto)
      throws PackageOverweightException {
    Parcel parcel = new Parcel(
        new Dimensions(parcelRequestDto.getWeight(), parcelRequestDto.getHeight(),
            parcelRequestDto.getLength(), parcelRequestDto.getWidth()));

    ParcelConditionStatus parcelConditionStatus = parcelConditionCheckerService.checkParcelCondition(
        parcel);
    if (parcelConditionStatus.equals(ParcelConditionStatus.REJECT)) {
      throw new PackageOverweightException(ExceptionMessage.PACKAGE_OVERWEIGHT);
    }

    return parcelCostCalculatorService.calculate(parcel, parcelConditionStatus);
  }
}
