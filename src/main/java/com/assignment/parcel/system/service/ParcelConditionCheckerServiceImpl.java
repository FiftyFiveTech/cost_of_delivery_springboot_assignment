package com.assignment.parcel.system.service;

import com.assignment.parcel.system.config.ParcelConditionsConfig;
import com.assignment.parcel.system.constants.ParcelConditionStatus;
import com.assignment.parcel.system.model.Parcel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParcelConditionCheckerServiceImpl implements ParcelConditionCheckerService {

  @Autowired
  private ParcelConditionsConfig parcelConditionsConfig;

  @Override
  public ParcelConditionStatus checkParcelCondition(Parcel parcel) {

    if (parcel.getDimensions().getWeight() > parcelConditionsConfig.getParcelMaxWeight()) {
      return ParcelConditionStatus.REJECT;
    } else if (parcel.getDimensions().getWeight()
        > parcelConditionsConfig.getHeavyParcelMinWeight()) {
      return ParcelConditionStatus.HEAVY_PARCEL;
    } else if (parcel.getDimensions().getVolume()
        <= parcelConditionsConfig.getSmallParcelMaxVolume()) {
      return ParcelConditionStatus.SMALL_PARCEL;
    } else if (parcel.getDimensions().getVolume()
        <= parcelConditionsConfig.getMediumParcelMaxVolume()) {
      return ParcelConditionStatus.MEDIUM_PARCEL;
    } else {
      return ParcelConditionStatus.LARGE_PARCEL;
    }
  }

}
