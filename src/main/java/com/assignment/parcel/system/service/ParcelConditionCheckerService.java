package com.assignment.parcel.system.service;

import com.assignment.parcel.system.constants.ParcelConditionStatus;
import com.assignment.parcel.system.model.Parcel;

public interface ParcelConditionCheckerService {

  ParcelConditionStatus checkParcelCondition(Parcel parcel);

}
