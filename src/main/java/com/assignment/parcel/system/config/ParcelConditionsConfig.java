package com.assignment.parcel.system.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties
public class ParcelConditionsConfig {

  @Value("${heavy.parcel.unit.price}")
  private Double heavyParcelUnitPrice;

  @Value("${small.parcel.unit.price}")
  private Double smallParcelUnitPrice;

  @Value("${medium.parcel.unit.price}")
  private Double mediumParcelUnitPrice;

  @Value("${large.parcel.unit.price}")
  private Double largeParcelUnitPrice;

  @Value("${parcel.max.weight}")
  private Double parcelMaxWeight;

  @Value("${heavy.parcel.min.weight}")
  private Double heavyParcelMinWeight;

  @Value("${small.parcel.max.volume}")
  private Double smallParcelMaxVolume;

  @Value("${medium.parcel.max.volume}")
  private Double mediumParcelMaxVolume;

}
