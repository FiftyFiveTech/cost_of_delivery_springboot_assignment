package com.assignment.parcel.system.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ExceptionMessage {

  public static final String PACKAGE_OVERWEIGHT = "Package cannot weight more than 60kg";
  public static final String NUMERIC_DIMENSIONS = "All dimensions should be numeric";
}
