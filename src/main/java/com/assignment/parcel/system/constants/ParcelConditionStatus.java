package com.assignment.parcel.system.constants;

public enum ParcelConditionStatus {
  REJECT, HEAVY_PARCEL, SMALL_PARCEL, MEDIUM_PARCEL, LARGE_PARCEL
}
