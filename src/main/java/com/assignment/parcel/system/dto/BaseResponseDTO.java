package com.assignment.parcel.system.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class BaseResponseDTO {

  private int status;
  private String message;
}
