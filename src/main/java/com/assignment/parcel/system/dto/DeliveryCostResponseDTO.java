package com.assignment.parcel.system.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class DeliveryCostResponseDTO {

  private Double cost;

}
