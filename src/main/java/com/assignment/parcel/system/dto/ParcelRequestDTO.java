package com.assignment.parcel.system.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParcelRequestDTO {

  @NotNull(message = "Weight is required")
  @Min(value = 1, message = "Weight cannot be less than 1")
  private Double weight;

  @NotNull(message = "Height is required")
  @Min(value = 1, message = "Height cannot be less than 1")
  private Double height;

  @NotNull(message = "width is required")
  @Min(value = 1, message = "Width cannot be less than 1")
  private Double width;

  @NotNull(message = "length is required")
  @Min(value = 1, message = "Length cannot be less than 1")
  private Double length;
}
