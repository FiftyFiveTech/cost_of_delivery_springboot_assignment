package com.assignment.parcel.system.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Parcel {

  private Dimensions dimensions;

  @Data
  @AllArgsConstructor
  public static class Dimensions {

    private Double weight;
    private Double height;
    private Double length;
    private Double width;
    private Double volume;

    public Dimensions(Double weight, Double height, Double length, Double width) {
      this.setWeight(weight);
      this.setHeight(height);
      this.setLength(length);
      this.setWidth(width);
      this.setVolume(this.getHeight() * this.getLength() * this.getWidth());
    }
  }

}
