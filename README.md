# Parcel Cost Calculator

## Description

Create a spring boot application that provides an API as described below. The application should be clean, well-designed and maintainable.

## Instructions

Create an API that will calculate the cost of delivery of a parcel based on weight and volume (volume = height * width * length). The API should accept the following:
1. Weight(kg)
2. Height(cm)
3. Width(cm)
4. Length(cm)

## Implementation

Used Spring Boot framework with Java for creating Backend services. Here created a `ParcelRequestDto` that accepts the dimensions of the parcel. The `ParcelRequestDto` is then sent to the services to fetch the `ParcelConditionStatus` (RuleName) and thereby get the `cost` of delivery depending upon the conditions given.

Added unit test cases for controller and services to check if functions behave in the way we predict under multiple scenarios.

## Folder Structure

- `controller` : Consists of all classes that expose api.
- `service` : Consists of business logic to calculae cost of delivery.
- `model` : Consists of all the models.
- `config` : Consist of configuration to load all parcel conditions values from application properties and enables cors access.

## How to Run

- Clone the Git repository
- Run `mvn clean install`
- Run `mvn spring-boot:run` or `java -jar target/parcel-system-0.0.1-SNAPSHOT.jar`